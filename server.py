
import os
from http.server import BaseHTTPRequestHandler
from routes.router import routes
from response.templateHandler import TemplateHandler
from response.badRequestHandler import BadRequestHandler
from response.staticHandler import StaticHandler



class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        return

    def do_POST(self):
        return

    def do_GET(self):
        split_path = os.path.splitext(self.path)
        request_extension = split_path[1]
        if request_extension is "" or request_extension is ".html":
            if self.path in routes:
                handler = TemplateHandler()
                handler.find(routes[self.path])
            else:
                handler = BadRequestHandler()
        elif request_extension is ".py":
            handler = BadRequestHandler()

        else:
            handler = StaticHandler()
            handler.find(self.path)

        self.respond({
            'handler': handler
        })


    '''
    def do_GET(self):
    self.respond()

    def handle_http(self, status, content_type):
    self.send_response(status)
    self.send_header('Content-type', content_type)
    self.end_headers()
    route_content = routes[self.path]
    return bytes(route_content, "UTF-8")

    def handle_http(self, handler):

      status = 200
      content_type ="text/plain"
      response_content = ""

      if self.path  in routes:
          print(routes[self.path])
          route_content =routes[self.path]['template']
          filepath = Path("templates/{}".format(route_content))
          if filepath.is_file():
              content_type = "text/html"
              response_content = open("templates/{}".format(route_content))
              response_content = response_content.read()
          else:
              content_type = "text/plain"
              response_content = "404 Not found"
      else:
          content_type = "text/plain"
          response_content = "404 Not Found"

      self.send_response(status)
      self.send_header('Content-type', content_type)
      self.end_headers()
      return bytes(response_content, "UTF-8")
      '''
    def handle_http(self, handler):
        status_code = handler.getStatus()
        self.send_response(status_code)

        if status_code is 200:
            content = handler.getContents()
            self.send_header('Content-type', handler.getContentType())
        else:
            content = "404 Not found."

        self.end_headers()

        if isinstance( content, (bytes, bytearray) ):
            return content

        return bytes(content, 'UTF-8')

    '''
    def respond(self):
        content = self.handle_http()
        self.wfile.write(content)
    '''
    def respond(self, opts):
        response = self.handle_http(opts['handler'])
        self.wfile.write(response)
